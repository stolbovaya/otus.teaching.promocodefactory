﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        public Task<IEnumerable<T>> DeleteByIdAsync(Guid id)
        {
            Data = Data.Where(x => x.Id != id).ToList();

            return Task.FromResult(Data);
        }

            public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }
        public Task<T> SetAsync(T newValue)
        {
            Data = Data.Select(i => i.Id == newValue.Id ? newValue : i);
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == newValue.Id));
        }
        public Task<T> NewAsync(T newValue)
        {
            Data = Data.Append(newValue);
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == newValue.Id));
        }
    }
}